package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class welcome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_out_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_out_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_out_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n");
      out.write("        <meta name=\"description\" content=\"\">\r\n");
      out.write("        <meta name=\"author\" content=\"\">\r\n");
      out.write("        <title>Profile </title>\r\n");
      out.write("        <!-- Bootstrap core CSS-->\r\n");
      out.write("        <link href=\"ressources/vendor/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("        <!-- Custom fonts for this template-->\r\n");
      out.write("        <link href=\"ressources/vendor/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\r\n");
      out.write("        <!-- Page level plugin CSS-->\r\n");
      out.write("        <link href=\"ressources/vendor/datatables/dataTables.bootstrap4.css\" rel=\"stylesheet\">\r\n");
      out.write("        <!-- Custom styles for this template-->\r\n");
      out.write("        <link href=\"ressources/css/sb-admin.css\" rel=\"stylesheet\">\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body class=\"fixed-nav sticky-footer bg-dark\" id=\"page-top\">\r\n");
      out.write("        <!-- Navigation-->\r\n");
      out.write("        <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\" id=\"mainNav\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"welcome\">Social Network</a>\r\n");
      out.write("            <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("            </button>\r\n");
      out.write("            <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\r\n");
      out.write("                <ul class=\"navbar-nav navbar-sidenav\" id=\"exampleAccordion\">\r\n");
      out.write("                    <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Dashboard\">\r\n");
      out.write("                        <a class=\"nav-link\" href=\"welcome\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-dashboard\"></i>\r\n");
      out.write("                            <span class=\"nav-link-text\">My Profile</span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\r\n");
      out.write("                        <a class=\"nav-link\" href=\"#\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-area-chart\"></i>\r\n");
      out.write("                            <span class=\"nav-link-text\">Pending Posts</span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Tables\">\r\n");
      out.write("                        <a class=\"nav-link\" href=\"information\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-table\"></i>\r\n");
      out.write("                            <span class=\"nav-link-text\">Information</span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Components\">\r\n");
      out.write("                        <a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseComponents\" data-parent=\"#exampleAccordion\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-wrench\"></i>\r\n");
      out.write("                            <span class=\"nav-link-text\">Settings</span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                        <ul class=\"sidenav-second-level collapse\" id=\"collapseComponents\">\r\n");
      out.write("                            <li>\r\n");
      out.write("                                <a href=\"#\">Navbar</a>\r\n");
      out.write("                            </li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                <ul class=\"navbar-nav sidenav-toggler\">\r\n");
      out.write("                    <li class=\"nav-item\">\r\n");
      out.write("                        <a class=\"nav-link text-center\" id=\"sidenavToggler\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-angle-left\"></i>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                <ul class=\"navbar-nav ml-auto\">\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"messagesDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-envelope\"></i>\r\n");
      out.write("                            <span class=\"d-lg-none\">Messages\r\n");
      out.write("                                <span class=\"badge badge-pill badge-primary\">12 New</span>\r\n");
      out.write("                            </span>\r\n");
      out.write("                            <span class=\"indicator text-primary d-none d-lg-block\">\r\n");
      out.write("                                <i class=\"fa fa-fw fa-circle\"></i>\r\n");
      out.write("                            </span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                        <div class=\"dropdown-menu\" aria-labelledby=\"messagesDropdown\">\r\n");
      out.write("                            <h6 class=\"dropdown-header\">New Messages:</h6>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <strong>David Miller</strong>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <strong>Jane Smith</strong>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <strong>John Doe</strong>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item small\" href=\"#\">View all messages</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"alertsDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-bell\"></i>\r\n");
      out.write("                            <span class=\"d-lg-none\">Alerts\r\n");
      out.write("                                <span class=\"badge badge-pill badge-warning\">6 New</span>\r\n");
      out.write("                            </span>\r\n");
      out.write("                            <span class=\"indicator text-warning d-none d-lg-block\">\r\n");
      out.write("                                <i class=\"fa fa-fw fa-circle\"></i>\r\n");
      out.write("                            </span>\r\n");
      out.write("                        </a>\r\n");
      out.write("                        <div class=\"dropdown-menu\" aria-labelledby=\"alertsDropdown\">\r\n");
      out.write("                            <h6 class=\"dropdown-header\">New Alerts:</h6>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <span class=\"text-success\">\r\n");
      out.write("                                    <strong>\r\n");
      out.write("                                        <i class=\"fa fa-long-arrow-up fa-fw\"></i>Status Update</strong>\r\n");
      out.write("                                </span>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <span class=\"text-danger\">\r\n");
      out.write("                                    <strong>\r\n");
      out.write("                                        <i class=\"fa fa-long-arrow-down fa-fw\"></i>Status Update</strong>\r\n");
      out.write("                                </span>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item\" href=\"#\">\r\n");
      out.write("                                <span class=\"text-success\">\r\n");
      out.write("                                    <strong>\r\n");
      out.write("                                        <i class=\"fa fa-long-arrow-up fa-fw\"></i>Status Update</strong>\r\n");
      out.write("                                </span>\r\n");
      out.write("                                <span class=\"small float-right text-muted\">11:21 AM</span>\r\n");
      out.write("                                <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <div class=\"dropdown-divider\"></div>\r\n");
      out.write("                            <a class=\"dropdown-item small\" href=\"#\">View all alerts</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item\">\r\n");
      out.write("                        <form action=\"searchUser\" method=\"GET\" class=\"form-inline my-2 my-lg-0 mr-lg-2\">\r\n");
      out.write("                            <div class=\"input-group\">\r\n");
      out.write("                                <input  name=\"name\" class=\"form-control\" type=\"text\" placeholder=\"Search for...\">\r\n");
      out.write("                                <span class=\"input-group-btn\">\r\n");
      out.write("                                    <button class=\"btn btn-primary\" type=\"submit\">\r\n");
      out.write("                                        <i class=\"fa fa-search\"></i>\r\n");
      out.write("                                    </button>\r\n");
      out.write("                                </span>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    <li class=\"nav-item\">\r\n");
      out.write("                        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n");
      out.write("                            <i class=\"fa fa-fw fa-sign-out\"></i>Logout</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </div>\r\n");
      out.write("        </nav>\r\n");
      out.write("        <div class=\"content-wrapper\">\r\n");
      out.write("            <div class=\"container-fluid\">\r\n");
      out.write("                <!-- Breadcrumbs-->\r\n");
      out.write("                <ol class=\"breadcrumb\">\r\n");
      out.write("                    <li class=\"breadcrumb-item\">\r\n");
      out.write("                        <a href=\"#\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.getUser_Firstname()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(' ');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.getUser_Lastname()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"breadcrumb-item active\">Profile</li>\r\n");
      out.write("                </ol>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("            <!-- /.container-fluid-->\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <div class=\"col-lg-11\">\r\n");
      out.write("                    <!-- Card Columns Example Social Feed-->\r\n");
      out.write("                    <div class=\"mb-0 mt-4\">\r\n");
      out.write("                        <i class=\"fa fa-newspaper-o\"></i>  My Wall</div>\r\n");
      out.write("                    <hr class=\"mt-2\">\r\n");
      out.write("                    <div class=\"\">\r\n");
      out.write("                        ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- /Card Columns-->\r\n");
      out.write("                </div>\r\n");
      out.write("                <!--content-wrapper-->\r\n");
      out.write("                <footer class=\"sticky-footer\">\r\n");
      out.write("                    <!--<div class=\"container\">\r\n");
      out.write("                      <div class=\"text-center\">\r\n");
      out.write("                        <small>Copyright © Your Website 2017</small>\r\n");
      out.write("                      </div>\r\n");
      out.write("                    </div>-->\r\n");
      out.write("                </footer>\r\n");
      out.write("                <!-- Scroll to Top Button-->\r\n");
      out.write("                <a class=\"scroll-to-top rounded\" href=\"#page-top\">\r\n");
      out.write("                    <i class=\"fa fa-angle-up\"></i>\r\n");
      out.write("                </a>\r\n");
      out.write("                <!-- Logout Modal-->\r\n");
      out.write("                <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n");
      out.write("                    <div class=\"modal-dialog\" role=\"document\">\r\n");
      out.write("                        <div class=\"modal-content\">\r\n");
      out.write("                            <div class=\"modal-header\">\r\n");
      out.write("                                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ready to Leave?</h5>\r\n");
      out.write("                                <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n");
      out.write("                                    <span aria-hidden=\"true\">×</span>\r\n");
      out.write("                                </button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"modal-body\">Select \"Logout\" below if you are ready to end your current session.</div>\r\n");
      out.write("                            <div class=\"modal-footer\">\r\n");
      out.write("                                <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancel</button>\r\n");
      out.write("                                <a class=\"btn btn-primary\" href=\"logout\">Logout</a>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <!-- Bootstrap core JavaScript-->\r\n");
      out.write("                <script src=\"ressources/vendor/jquery/jquery.min.js\"></script>\r\n");
      out.write("                <script src=\"ressources/vendor/bootstrap/js/bootstrap.bundle.min.js\"></script>\r\n");
      out.write("                <!-- Core plugin JavaScript-->\r\n");
      out.write("                <script src=\"ressources/vendor/jquery-easing/jquery.easing.min.js\"></script>\r\n");
      out.write("                <!-- Page level plugin JavaScript-->\r\n");
      out.write("                <script src=\"ressources/vendor/chart.js/Chart.min.js\"></script>\r\n");
      out.write("                <script src=\"ressources/vendor/datatables/jquery.dataTables.js\"></script>\r\n");
      out.write("                <script src=\"ressources/vendor/datatables/dataTables.bootstrap4.js\"></script>\r\n");
      out.write("                <!-- Custom scripts for all pages-->\r\n");
      out.write("                <script src=\"ressources/js/sb-admin.min.js\"></script>\r\n");
      out.write("                <!-- Custom scripts for this page-->\r\n");
      out.write("                <script src=\"ressources/js/sb-admin-datatables.min.js\"></script>\r\n");
      out.write("                <script src=\"ressources/js/sb-admin-charts.min.js\"></script>\r\n");
      out.write("            </div>\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${posts}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("post");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                            <!-- Example Social Card-->\r\n");
          out.write("                            <div class=\"card mb-3\">\r\n");
          out.write("                                <a href=\"#\">\r\n");
          out.write("                                    <img class=\"card-img-top img-fluid w-100\" src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.getPost_Image()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" alt=\"\">\r\n");
          out.write("                                </a>\r\n");
          out.write("                                <div class=\"card-body\">\r\n");
          out.write("                                    <h5 class=\"card-title mb-1\">");
          if (_jspx_meth_c_out_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("</h5>\r\n");
          out.write("                                    <p class=\"card-text small\">");
          if (_jspx_meth_c_out_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\r\n");
          out.write("                                    </p>\r\n");
          out.write("                                </div>\r\n");
          out.write("                                <hr class=\"my-0\">\r\n");
          out.write("                                <div class=\"card-body py-2 small\">\r\n");
          out.write("                                    <a class=\"mr-3 d-inline-block\" href=\"#\">\r\n");
          out.write("                                        <i class=\"fa fa-fw fa-thumbs-up\"></i>Like</a>\r\n");
          out.write("                                    <a class=\"mr-3 d-inline-block\" href=\"#\">\r\n");
          out.write("                                        <i class=\"fa fa-fw fa-thumbs-down\"></i>Dislike</a>\r\n");
          out.write("                                    <a class=\"d-inline-block\" href=\"#\">\r\n");
          out.write("                                        <i class=\"fa fa-fw fa-bell\"></i>Report</a>\r\n");
          out.write("                                </div>\r\n");
          out.write("                                <div class=\"card-footer small text-muted\">Posted a while ago</div>\r\n");
          out.write("                            </div>\r\n");
          out.write("                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_out_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_0 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_0.setPageContext(_jspx_page_context);
    _jspx_th_c_out_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.getPost_Status()}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_0 = _jspx_th_c_out_0.doStartTag();
    if (_jspx_th_c_out_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
    return false;
  }

  private boolean _jspx_meth_c_out_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_1 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_1.setPageContext(_jspx_page_context);
    _jspx_th_c_out_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${post.getPost_Text()}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_1 = _jspx_th_c_out_1.doStartTag();
    if (_jspx_th_c_out_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_1);
    return false;
  }
}
