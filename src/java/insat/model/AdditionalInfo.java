/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.model;

/**
 *
 * @author Madou
 */
public class AdditionalInfo {

    private String user_Identity;

    private String user_skills;

    private String user_contact;

    private String user_hobbies;

    public String getUser_Identity() {
        return user_Identity;
    }

    public void setUser_Identity(String user_Identity) {
        this.user_Identity = user_Identity;
    }

    public String getUser_skills() {
        return user_skills;
    }

    public void setUser_skills(String user_skills) {
        this.user_skills = user_skills;
    }

    public String getUser_contact() {
        return user_contact;
    }

    public void setUser_contact(String user_contact) {
        this.user_contact = user_contact;
    }

    public String getUser_hobbies() {
        return user_hobbies;
    }

    public void setUser_hobbies(String user_hobbies) {
        this.user_hobbies = user_hobbies;
    }

}
